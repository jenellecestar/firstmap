//
//  ViewController.swift
//  MyFirstMap
//
//  Created by MacStudent on 2018-07-19.
//  Copyright © 2018 MacStudent. All rights reserved.
//

import UIKit
import MapKit  // 1. Apple's Map Library

import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    // MARK - outlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var label: UILabel!
    
    
    var myLocations : [CLLocationCoordinate2D] = []
    
    
    // Make a CoreLocation variable
    var manager : CLLocationManager!

    override func viewDidLoad() {
        super.viewDidLoad()

        // 1. Setup your CoreLocation variable
        self.manager = CLLocationManager()
        self.manager.delegate = self
        
        // 2. Tell ios how accurate you want the location to be
        self.manager.desiredAccuracy = kCLLocationAccuracyBest
        
        // 3. Ask the user for permission to get their location
        self.manager.requestAlwaysAuthorization()
        
        // 4. Get the user's location
        self.manager.startUpdatingLocation()
        
        // UI Nonsense - Setup the map to SHOW their location
        mapView.delegate = self
        mapView.mapType = MKMapType.standard
        mapView.showsUserLocation = true    //blinking blue dot!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // ----------------------------------------
    // MARK - Corelocation related functions
    // ----------------------------------------
   
    // This function gets run every time the person moves
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // OPTIONAL: get the person's most recent location
        //let i = locations.count - 1
        //label.text = "\(locations[i])"
        
        self.myLocations.append(mapView.userLocation.coordinate)
        
        //myLocations.append(locations[i] as CLLocation)
        
        if (self.myLocations.count > 2) {
            let polyline = MKPolyline(coordinates:self.myLocations, count:self.myLocations.count)
            self.mapView.add(polyline)
            
        }
        
        
        // UI Nonsense: update the map to match the person's location
        // You need:
        //  -- coordinate - center of map position
        //  -- span - zoom level
        //  -- region - nonsense
        let coord = mapView.userLocation.coordinate
        let span = MKCoordinateSpanMake(0.01, 0.01)
        let region = MKCoordinateRegionMake(coord, span)
        self.mapView.setRegion(region, animated: true)
        
    }
    
    // -------------------------------------------------
    // MARK: Polyline Functions
    // -------------------------------------------------
    //NONSENSE UI FUNCTION TO SHOW GRAPHICS  ON MAPS
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if (overlay is MKPolyline) {
            // 4. show polyline on the map
            let r = MKPolylineRenderer(overlay: overlay)
            
            // configure the line
            // How do you want the line to look?
            r.strokeColor = UIColor.red     // line color
            r.lineWidth = 5                 // line thickness
            
            return r
        }
        return MKOverlayRenderer()
    }
    
    
    
    
    
    
    
    
    


}

